<#
.SYNOPSIS
    Creates the configuration file for the Get-Wallpaper script
.DESCRIPTION
    This function creates a configuration file for the Get-Wallpaper script
    The file is created in $HOME\.config\Get-Wallpaper\
.EXAMPLE
    Setup-Wallpaper
    This command is used to create the settings file
.INPUTS
    There are no inputs
.OUTPUTS
    outputs are in $HOME\.config\Get-Wallpaper\config.ini
.NOTES
    This script needs Get-ScriptConfig to work.
    Use the following command to install it:
    Install-Module ScriptConfig [-Scope {CurrentUser | AllUsers}]
#>
function Setup-Wallpaper{
    BEGIN{
        $confPath = "$HOME\.config\Get-Wallpaper\config.ini"
        $configDir = Split-Path $confPath
        $cnfgDirTest = Test-Path $configDir
                }
    Process{
        if ($cnfgDirTest -eq $true) {
            notepad.exe $confPath
        }
        else{
            $UserPath = Read-Host -Prompt "Which directory do you want to place the wallpapers"
            $UserRes = Read-Host "
            Which Resolution do you want?
            320x480,
            640x480,	800x480,	800x600,	1024x768,
            1024x1024,	1152x864,	1280x720,	1280x800,
            1280x960,	1280x1024,	1366x768,	1400x1050,
            1440x900,	1600x1200,	1680x1050,	1680x1200,
            1920x1080,	1920x1200,	1920x1440,	2560x1440
            "
            $TextArray = "; WorkingDir:   Storage folder for the wallpapers ", "; Example       WORKINGDIR=C:\Users\<USER>\Pictures\wallpaper\", "; ", "; Resolution:   Specify the desired Resolution", "; Example       RESOLUTION=2560x1440", "; ", "; Valid options (not always available)", "; 320x480	 640x480	 800x480	 800x600	 1024x768	", "; 1024x1024	 1152x864	 1280x720	 1280x800	", "; 1280x960	 1280x1024	 1366x768	 1400x1050	 ", "; 1440x900	 1600x1200	 1680x1050	 1680x1200	 ", "; 1920x1080	 1920x1200	 1920x1440	 2560x1440", "", ";=====EDIT BELOW THIS LINE=====", "", "WorkingDir=$UserPath", "Resolution=$UserRes"
            $TextArray | Out-File -FilePath $confPath -Encoding utf8 -NoClobber
            notepad.exe $confPath
        }
    }
}



<#
.SYNOPSIS
    Downloads all wallpapers of the specified resolution and sorts them by download month
.DESCRIPTION
    Powershell function to download the wallpapers form SmashingMagazine's Montly wallpaper posts.
    The script creates a folder structure based on month and year, and sorts the wallpapers with and without calendars
.EXAMPLE
    PS C:\> Get-Wallpaper https://www.smashingmagazine.com/YEAR/PREVIOUSMONTH/desktop-wallpaper-calendars-MONTH-YEAR/
    Saves wallpapers in $WorkingDir/$AD/<CURRENT MONTH>
    The input URL must be the one of a Desktop wallpaper post
.EXAMPLE
    PS C:\> Get-Wallpaper https://www.smashingmagazine.com/YEAR/PREVIOUSMONTH/desktop-wallpaper-calendars-MONTH-YEAR/ -NextMonth
    Saves wallpapers in $WorkingDir/$AD/<NEXT MONTH>
    The input URL must be the one of a Desktop wallpaper post
.INPUTS
    System.String
    You can pipe a string that contains a URL
.OUTPUTS
    Outputs two folders with wallpapers, sorted by the wallpaper containg a calendar on it or not
.NOTES
    This script needs Get-ScriptConfig to work.
    Use the following command to install it:
    Install-Module ScriptConfig [-Scope {CurrentUser | AllUsers}]
    #>
function Get-Wallpaper{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $True,
        ValueFromPipeline = $True)]
        [System.URI]$Url,
        [switch]$NextMonth = $false,
        [switch]$OnlyCal = $false
    )

    BEGIN{
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        $config = Get-ScriptConfig -Path $Home\.config\Get-Wallpaper\config.ini -Format INI
        $WorkingDir = $config.WorkingDir
        $Resolution = $config.Resolution
        $AD = Get-Date -Format 'yyyy'
        $CM = Get-Date -Format 'MM'
        $NM = '0' + [string]([INT]$CM + 1)
        $year = Test-Path $WorkingDir\$AD
        $wall = Test-Path $WorkingDir
        $i = 0
    }

    Process{
        Push-Location .
        If ($wall -eq $False){
            Split-Path $WorkingDir | Set-Location
            New-Item -ItemType Directory wallpaper
         }

        If ($year -eq $True)
        {
        Set-Location $WorkingDir\$AD
        }
        Else
        {
            New-Item -ItemType Directory $WorkingDir\$AD
            Set-Location $WorkingDir\$AD
        }


        if ($NextMonth) {
            New-Item -ItemType Directory $NM
            Set-Location $NM
        }
        else {
            New-Item -ItemType Directory $CM
            Set-Location $CM
        }
        New-Item -ItemType Directory cal, no-cal
        New-Item -ItemType File '.\url.txt'

        $response = Invoke-WebRequest -UseBasicParsing -Uri $url
        $parse = @(($response.Links).href)
        if ($OnlyCal) {
            $urls = $parse | Where-Object {$_} | ForEach-Object { if ($_.Contains("-cal-" + $Resolution)){$_}}
        }
        else {
           $urls = ($parse | Where-Object {$_} | ForEach-Object { if ($_.Contains($Resolution)){$_}})
        }
        
        $num = $urls.Length

        $names = @(foreach ($link in $urls) {
                $collection = $link -split "/"
               $collection[7]
        })

        while ($num -gt $i) {
           Invoke-WebRequest -Uri $urls[$i] -OutFile $names[$i]
           $i++
        }
    }

    END{
        if ($OnlyCal){
            Get-ChildItem -Filter "*-cal-*" | Move-Item -destination .\cal\
        }
        else {
            Get-ChildItem -Filter "*-nocal-*" | Move-Item -destination .\no-cal\
            Get-ChildItem -Filter "*-cal-*" | Move-Item -destination .\cal\
        }

        $urls | Out-File -FilePath .\url.txt -Encoding utf8
        explorer.exe .
        Pop-Location
    }

}
