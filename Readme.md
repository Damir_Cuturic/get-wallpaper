# Get-Wallpaper

    Downloads all wallpapers of the specified resolution and sorts them by download month

## Description

    Powershell function to download the wallpapers form SmashingMagazine's Montly wallpaper posts.
    The script creates a folder structure based on month and year, and sorts the wallpapers with and without calendars

## Example

```
PS C:\> Get-Wallpaper https://www.smashingmagazine.com/YEAR/PREVIOUSMONTH/desktop-wallpaper-calendars-MONTH-YEAR/
The input URL must be the one of a Desktop wallpaper post

```

```
PS C:\> Get-Wallpaper https://www.smashingmagazine.com/YEAR/PREVIOUSMONTH/desktop-wallpaper-calendars-MONTH-YEAR/ -NextMonth
The input URL must be the one of a Desktop wallpaper post
The -NextMonth switch adds 1 to the current month great for early birds
```

## TO-DO
[x] add -nextMonth switch which adds +1 to the current month (if you get the wallpapers early)

[x] Create new switch -OnlyCal to relpace ONLYCAL in config file

[ ] create external help files

[ ] Search what the best practices are regarding [Net.ServicePointManager]::SecurityProtocol

## Version

Version | Comment | Date
---------|----------|---------
0.0.1 | Initial Commit | 24-02-2018
0.0.2 | Added TLS1.2 certificate validation | 02-07-2018
0.0.3 | Added -NextMonth Switch | 02-07-2018
0.0.4 | Fixed code order in Setup-Wallpaper & Removed OnlyCal code | 03-07-2018
0.0.5 | Added -OnlyCal Switch | 03-07-2018
